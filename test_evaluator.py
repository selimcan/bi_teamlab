#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from evaluator import *
from tagger import *

S0 = Sentence()

S1 = Sentence()
S1.words = [Word("Time", "NN", "NN"), Word("flies", "VB", "NNS"),
            Word("like", "IN", "IN"), Word("an", "DT", "DT"),
            Word("arrow", "NN", "NN"), Word(".", ".", ".")]

S2 = Sentence()
S2.words = [Word("Fruit", "NN", "NN"), Word("flies", "NN", "VBZ"),
            Word("like", "VB", "IN"), Word("a", "DT", "DT"),
            Word("banana", "NN", "NN"), Word(".", ".", ".")]

C1 = Collection([S1, S2], ["NN", "VB", "NNS", "IN", "DT", ".", "VBZ"])

#         TP  TN   FP  FN    Precision  Recall  F1
# NN      4   7    0   1     1          0.8     0.8889
# VB      0   10   0   2     0          0       0
# NNS     0   11   1   0     0          0       0
# IN      1   10   1   0     0.5        1       0.6667
# DT      2   10   0   0     1          1       1
# .       2   10   0   0     1          1       1
# VBZ     0   11   1   0     0          0       0
#
# Total:  9   3   69   3

# Macro-averaged stats = 11÷12 + 10÷12 + 11÷12 + 11÷12 + 12÷12 + 12÷12 + 11÷12 = 0.9286

# "Note that macro-accuracy and micro-accuracy will always give the same number."
# (which is the case here).
# See: http://search.cpan.org/~kwilliams/Statistics-Contingency-0.09/lib/Statistics/Contingency.pm

class EvaluatorTests(unittest.TestCase):

    def test_micro_averaged_stats(self):
        self.assertAlmostEqual(micro_averaged_stats(C1)[0], 0.7500, places=4)
        self.assertAlmostEqual(micro_averaged_stats(C1)[1], 0.7500, places=4)
        self.assertAlmostEqual(micro_averaged_stats(C1)[2], 0.7500, places=4)
        self.assertAlmostEqual(micro_averaged_stats(C1)[3], 0.9286, places=4)

    def test_macro_averaged_stats(self):
        self.assertAlmostEqual(macro_averaged_stats(C1)[0], 0.5000, places=4)
        self.assertAlmostEqual(macro_averaged_stats(C1)[1], 0.5429, places=4)
        self.assertAlmostEqual(macro_averaged_stats(C1)[2], 0.5079, places=4)
        self.assertAlmostEqual(macro_averaged_stats(C1)[3], 0.9286, places=4)

    def test_calculate_prfa(self):
        self.assertAlmostEqual(calculate_prfa(numpy.array([9, 69, 3, 3]))[0], 0.7500, places=4)
        self.assertAlmostEqual(calculate_prfa(numpy.array([9, 69, 3, 3]))[1], 0.7500, places=4)
        self.assertAlmostEqual(calculate_prfa(numpy.array([9, 69, 3, 3]))[2], 0.7500, places=4)
        self.assertAlmostEqual(calculate_prfa(numpy.array([9, 69, 3, 3]))[3], 0.9286, places=4)

    def test_accuracy(self):
        self.assertEqual(accuracy(0, 0, 13, 2), 0.0)
        self.assertAlmostEqual(accuracy(0, 5, 0, 1), 0.83, places=2)
        self.assertEqual(accuracy(2, 4, 0, 0), 1.0)

    def test_precision(self):
        self.assertEqual(precision(0, 0), 0.0)
        self.assertEqual(precision(0, 13), 0.0)
        self.assertEqual(precision(2, 8), 0.2)
        self.assertEqual(precision(56, 0), 1.0)

    def test_recall(self):
        self.assertEqual(recall(0, 0), 0.0)
        self.assertEqual(recall(0, 234), 0.0)
        self.assertEqual(recall(2, 18), 0.1)
        self.assertEqual(recall(56, 0), 1.0)

    def test_f_one(self):
        self.assertEqual(f_one(0.0, 0.0), 0.0)
        self.assertAlmostEqual(f_one(0.4, 0.2), 0.27, places=2)
        self.assertEqual(f_one(1.0, 1.0), 1.0)

    def test_word2string(self):
        self.assertEqual(word2string(S1.words[0]), "Time\tNN\tNN")
