#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from tagger import *
from perceptron import *

S0 = Sentence()

S1 = Sentence()
S1.words = [Word("Time", "NN", "NN"), Word("flies", "VB", "NNS"),
            Word("like", "IN", "IN"), Word("an", "DT", "DT"),
            Word("arrow", "NN", "NN"), Word(".", ".", ".")]

S2 = Sentence()
S2.words = [Word("Fruit", "NN", "NN"), Word("flies", "NN", "VBZ"),
            Word("like", "VB", "IN"), Word("a", "DT", "DT"),
            Word("banana", "NN", "NN"), Word(".", ".", ".")]

S3 = Sentence()
S3.words = [Word("Whoa", "IJ", "IJ")]

C1 = Collection([S1, S2], ["NN", "VB", "NNS", "IN", "DT", ".", "VBZ"])

C2 = Collection([S3], ["IJ"])

W1 = Word('talked', 'Verb', 'Noun')
W1.features = ["suffix=ed", "pre_word=have"]

W2 = Word('construction', 'Noun', 'Noun')
W2.features = ["suffix=ion", "pre_word=the"]

S4 = Sentence()
S4.add_word(W1)
S4.add_word(W2)

C3 = Collection([S4], ['Noun', 'Verb'])


class MultilabelPerceptronTests(unittest.TestCase):

    def setUp(self):
        self.MLP = MultiLabelPerceptron(C3)
        self.MLP.train()

    def test_classify(self):
        self.assertEqual(self.MLP.classify(["suffix=ed", "pre_word=have"]), "Verb")
        self.assertEqual(self.MLP.classify(["suffix=ion", "pre_word=the"]), "Noun")
        self.assertEqual(self.MLP.classify(["suffix=ies", "pre_word=the"]), "Noun")


class DotProductTests(unittest.TestCase):

    def test_dot_product(self):
        self.assertEqual(dot_product(["default", "first_boolean=0", "second_boolean=1"], {}),
                         INITIAL_WEIGHT + INITIAL_WEIGHT + INITIAL_WEIGHT)
        self.assertEqual(dot_product(["default", "first_boolean=0", "second_boolean=1"],
                                     {"default": 1, "first_boolean=0": 2, "second_boolean=1": 3}),
                         1 + 2 + 3)


class ChangeWeightsTests(unittest.TestCase):

    def test_increasing_weights(self):
        self.assertEqual(change_weights(["default", "prefix=inc", "suffix=ing", "length=10"],
                                        {},
                                        1),
                         {"default": 1, "prefix=inc": 1, "suffix=ing": 1, "length=10": 1})
        self.assertEqual(change_weights(["default", "prefix=inc", "suffix=ing"],
                                        {"default": 1, "prefix=inc": 0, "suffix=ing": -1, "caps": 3},
                                        1),
                         {"default": 2, "prefix=inc": 1, "suffix=ing": 0, "caps": 3})

    def test_decreasing_weights(self):
        self.assertEqual(change_weights(["default", "prefix=inc", "suffix=ing", "length=10"],
                                        {},
                                        -1),
                         {"default": -1, "prefix=inc": -1, "suffix=ing": -1, "length=10": -1})
        self.assertEqual(change_weights(["default", "prefix=inc", "suffix=ing"],
                                        {"default": 1, "prefix=inc": 0, "suffix=ing": -1, "caps": 3},
                                        -1),
                         {"default": 0, "prefix=inc": -1, "suffix=ing": -2, "caps": 3})


if __name__ == '__main__':
    unittest.main()
