#!/usr/bin/python3
# -*- coding: utf-8 -*-

## A program to evaluate the performance of both sequential and
## non-sequential classification/tagging models on a given collection.
## To test the module, run: python3 -m unittest test_evaluator

import numpy


## =================
## Calculating:


## Collection -> Float Float Float Float
def micro_averaged_stats(collection):
    """Return micro-averaged precision, recall, f-one and accuracy
    (i.e. first aggregate tp_tn_fp_fn values for all POSs, then calculate).
    """
    tp_tn_fp_fn = numpy.array([0.0, 0.0, 0.0, 0.0])
    for pos in collection.parts_of_speech:
        tp_tn_fp_fn += collection.generate_confusion_matrix(pos)
    precision, recall, f_one, accuracy = calculate_prfa(tp_tn_fp_fn)
    return precision, recall, f_one, accuracy


## Collection -> Float Float Float Float
def macro_averaged_stats(collection):
    """Return macro-averaged precision, recall, f-one and accuracy
    (i.e. first calculate them for each POS, then average).
    """
    total_stats = numpy.array([0.0, 0.0, 0.0, 0.0])
    for pos in collection.parts_of_speech:
        tp_tn_fp_fn = collection.generate_confusion_matrix(pos)
        total_stats += numpy.array(calculate_prfa(tp_tn_fp_fn))
    total_stats /= len(collection.parts_of_speech)
    return total_stats[0], total_stats[1], total_stats[2], total_stats[3]


## Collection -> Float Float Float Float
def seq_macro_averaged_stats(collection):
    """Return macro-averaged precision, recall, f-one and accuracy
    (i.e. first calculate them for each POS, then average).
    """
    total_stats = numpy.array([0.0, 0.0, 0.0, 0.0])
    for pos in collection.parts_of_speech:
        tp_tn_fp_fn = collection.sequential_confusion_matrix(pos)
        total_stats += numpy.array(calculate_prfa(tp_tn_fp_fn))
    total_stats /= len(collection.parts_of_speech)
    return total_stats[0], total_stats[1], total_stats[2], total_stats[3]


## Int Int Int Int -> Float Float Float Float
def calculate_prfa(tp_tn_fp_fn):
    """Return precision, recall, F1-score and accuracy given an array with
    (true positives, true negatives, false positives, false negatives).
    """
    tp, tn, fp, fn = tp_tn_fp_fn
    p = precision(tp, fp)
    r = recall(tp, fn)
    f = f_one(p, r)
    a = accuracy(tp, tn, fp, fn)
    return p, r, f, a


## Int Int Int Int -> Float
def accuracy(tp, tn, fp, fn):
    """Calculate accuracy given numbers for (true positives, true negatives,
    false positives, false negatives).
    """
    return (tp + tn) / (tp + fp +tn + fn)


## Int Int -> Float
def precision(tp, fp):
    """Calculate precision given numbers for (true positives, false positives)."""
    if tp + fp == 0.0:
        return 0.0
    else:
        return tp / (tp + fp)


## Int Int -> Float
def recall(tp, fn):
    """Calculate recall given numbers for (true positives, false negatives)."""
    if tp + fn == 0.0:
        return 0.0
    else:
        return tp / (tp + fn)


## Float Float -> Float
def f_one(precision, recall):
    """Calculate F1 score given values for precision and recall."""
    if precision + recall == 0.0:
        return 0.0
    else:
        return 2 * precision * recall / (precision + recall)


## =================
## Printing:


# TODO call printing functions either 'print' or 'output', for consistency.

## Collection String -> Void
def output_stats(collection, filename):
    """For each class in the collection, calculate precision, recall, F1 and
    accuracy and write them to the 'filename' file.
    """
    with open(filename, 'w') as statsOutput:
        statsOutput.write('Stats by POS: ' + '\n')
        for pos in collection.parts_of_speech:
            posStats = calculate_prfa(collection.generate_confusion_matrix(pos))
            statsOutput.write(' POS: ' + str(pos) +
                              ' Precision: ' + str(posStats[0]) +
                              ' Recall: ' + str(posStats[1]) +
                              ' F1: ' + str(posStats[2]) +
                              ' Accuracy: ' + str(posStats[3]) + '\n')


## Collection Filename -> Void
def output_pred_errors(collection, outputErrors):
    """Write words in the collection having wrong predicted tag to the 'outputErrors' file
    (together with their gold and predicted tags).
    """
    with open(outputErrors, 'w') as outputErrors:
        for word in collection:
            if not word.classified_correctly():
                outputErrors.write(word2string(word) + '\n')


## Collection Filename -> Void
def print_false_negatives(collection, fnFile):
    """For each label/class in the collection, write total # of false negatives for
    that class and the proportion of them to the 'fnFile'.
    """
    with open(fnFile, 'w') as fnFile:
        for label in collection.parts_of_speech:
            false_neg = collection.generate_confusion_matrix(label)[3]
            true_pos = collection.generate_confusion_matrix(label)[0]
            fnFile.write(label + '\t' +
                         'False Negative=' + str(false_neg) +'\t' +
                         'Total for POS=' + str(false_neg + true_pos) + '\t' +
                         'Proportion FN=' + str(false_neg/(false_neg + true_pos)) + '\n') # TODO: This is 1-recall actually.


## Collection Filename -> Void
def print_fn_examples(collection, fnexFile):
    """For each label/class in the collection, write words having wrong predicted
    tag to the 'fnexFile' (together with their gold and predicted tags).
    """
    with open(fnexFile, 'w') as fnexFile:
        for label in collection.parts_of_speech:
            for word in collection:
                if word.gold_tag == label and not word.classified_correctly():
                    fnexFile.write(word2string(word) + '\n')


## Collection -> Void
def print_named_entities(collection, nes_file):
    """Print all predicted named entities to nes_file, together with their labels,
    separated by tab, e.g.: Israel\tLOC.

    Then you can do e.g:
      - sort named_entities.txt | uniq -c | sort -nr | less
        (to get a frequency list of named entities) or
      - cut -f 1 named_entities.txt | sort | uniq -c | sort -nr | less
        (to ignore the label)

    ASSUME: - I tag never follows O tag."""
    nes_file = open(nes_file, 'w')    
#    with open(nes_file, 'w') as nes_file:
    ne = ''
    for word in collection:
        if word.pred_tag[1] == 'B':
            label = word.pred_tag[3:]
            ne += ' ' + word.name
        elif word.pred_tag[1] == 'I':
            ne += ' ' + word.name
        elif ne != '':
            nes_file.write(ne + '\t' + label +'\n')
            ne = ''
    nes_file.close()

## Word -> String
def word2string(word):
    """Return word.name, word.gold_tag and word.pred_tag separated by tab characters."""
    return word.name + '\t' + word.gold_tag + '\t' + word.pred_tag
