#!/usr/bin/python3
# -*- coding: utf-8 -*-

## Given a predictions file, print all predicted named entities
## together with their labels, separated by tab, e.g.: Israel\tLOC
##                                                     Angela Merkel\tPERS
##
## Then you can do e.g:
##   - sort named_entities.txt | uniq -c | sort -nr | less
##     (to get a frequency list of named entities) or
##   - cut -f 1 named_entities.txt | sort | uniq -c | sort -nr | less
##     (to ignore the label)
##
## ASSUME: - I tag never follows O tag."""
##
## USAGE: cat predictions.iob | python print_named_entities.py

import sys

entity = ''

for line in sys.stdin:
    line = line.strip()
    if line:
        word, tag = line.split('\t')
        if tag[1] == 'B':
            label = tag[3:]
            entity += ' ' + word
        elif tag[1] == 'I':
            entity += ' ' + word
        elif entity != '':
            print(entity + '\t' + label)
            entity = ''
