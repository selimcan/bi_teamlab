# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 08:42:44 2015

@author: blochb
"""

import tagger_seq
import perceptron_seq
import evaluator

#output_stats(collection, 'post-stats-output.txt')

# Print named entities works if I do this:
# ========================================

#trainCollection = tagger_seq.readFiles1('data/conll/large-train.iob')
#MLP = perceptron_seq.MultiLabelPerceptron(trainCollection)
#MLP.output_model('models/large-train-iob.txt')

#testCollection = tagger_seq.readFiles1('data/conll/dev.iob')
#perceptron_seq.model_predictions(testCollection, 'models/large-train-iob.txt', 'predictions/dev-pred.iob')

infile = '2015-07-06'
testCollection = tagger_seq.readFiles1('news/'+infile+'.iob')
perceptron_seq.model_predictions(testCollection, 'models/large-train-iob.txt', 'predictions/'+infile+'-pred.iob')
evaluator.print_named_entities(testCollection, 'predictions/nes-'+infile+'.txt')


# ==========================

#print(evaluator.calculate_prfa(testCollection.sequential_confusion_matrix()))
#evaluator.output_stats(trainCollection, 'stats-by-pos.txt')