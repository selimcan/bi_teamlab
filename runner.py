# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 08:42:44 2015

@author: blochb
"""

import tagger
import perceptron
import evaluator

#output_stats(collection, 'post-stats-output.txt')

trainCollection = tagger.readFiles1('data/train-and-dev.col')
MLP = perceptron.MultiLabelPerceptron(trainCollection)
MLP.output_model('models/train-and-dev-model.txt')

##evaluator.output_stats(trainCollection, 'stats-by-pos.txt')
#testCollection = tagger.readFiles1('data/dev.col')
#perceptron.model_predictions(testCollection, 'train-model.txt')

evaluator.output_stats(trainCollection, 'stats-by-pos.txt')


