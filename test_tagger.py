#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from tagger import *

## =================
## Constants:

S0 = Sentence()

S1 = Sentence()
S1.words = [Word("Time", "NN", "NN"), Word("flies", "VB", "NNS"),
            Word("like", "IN", "IN"), Word("an", "DT", "DT"),
            Word("arrow", "NN", "NN"), Word(".", ".", ".")]

S2 = Sentence()
S2.words = [Word("Fruit", "NN", "NN"), Word("flies", "NN", "VBZ"),
            Word("like", "VB", "IN"), Word("a", "DT", "DT"),
            Word("banana", "NN", "NN"), Word(".", ".", ".")]

S3 = Sentence()
S3.words = [Word("Whoa", "IJ", "IJ")]

C1 = Collection([S1, S2], ["NN", "VB", "NNS", "IN", "DT", ".", "VBZ"])
C2 = Collection([S3], ["IJ"])


class WordTests(unittest.TestCase):

    def setUp(self):
        self.W1 = Word("running", "VB", "VB")
        self.W2 = Word("running", "VB", "NN")
        self.W3 = Word("A", "DT", "DT")

    def test_classified_correctly(self):
        self.assertEqual(self.W1.classified_correctly(), True)
        self.assertEqual(self.W2.classified_correctly(), False)

    def test_extract_word_features(self):
        self.W1.extract_word_features()
        self.assertEqual(self.W1.features, ["default", "word=running", "suffix=ing", "prefix=run",
                                            "ALL_CAPS=False", "first_Cap=False"])
        self.W3.extract_word_features()
        self.assertEqual(self.W3.features, ["default", "word=A", "suffix=a", "prefix=a",
                                            "ALL_CAPS=True", "first_Cap=True"])


class SentenceTests(unittest.TestCase):

    def test_add_word(self):
        S0.add_word(Word(".", ".", "."))
        self.assertEqual(len(S0.words), 1)

    def test_extract_features(self):
        S3.extract_features()
        self.assertEqual(S3.words[0].features,
                         ["default", "word=Whoa", "suffix=hoa", "prefix=who", "ALL_CAPS=False",
                          "first_Cap=True", "pre_word_2=<s>", "pre_word_1=<s>", "post_word=</s>"])

    def test_generate_confusion_matrix(self):
        self.assertEqual(S1.generate_confusion_matrix("NN"), (2, 4, 0, 0))
        self.assertEqual(S1.generate_confusion_matrix("VB"), (0, 5, 0, 1))


class CollectionTests(unittest.TestCase):
    def test_generate_confusion_matrix(self):
        self.assertEqual(C1.generate_confusion_matrix("NN"), [4, 7, 0, 1])
        self.assertEqual(C1.generate_confusion_matrix("VB"), [0, 10, 0, 2])
        self.assertEqual(C1.generate_confusion_matrix("NNS"), [0, 11, 1, 0])
        self.assertEqual(C1.generate_confusion_matrix("IN"), [1, 10, 1, 0])
        self.assertEqual(C1.generate_confusion_matrix("DT"), [2, 10, 0, 0])
        self.assertEqual(C1.generate_confusion_matrix("."), [2, 10, 0, 0])
        self.assertEqual(C1.generate_confusion_matrix("VBZ"), [0, 11, 1, 0])
        # sum is: 9, 69, 3, 3

if __name__ == '__main__':
    unittest.main()
