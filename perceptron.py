#!/usr/bin/python3
# -*- coding: utf-8 -*-

## A multilabel perceptron-based classifier.

import evaluator


## =================
## Constants:


INITIAL_WEIGHT = 0.0
NUMBER_OF_ITERATIONS = 100


## =================
## Data definitions:


## MultilabelPerceptron's Prediction is a String.
## interp. label of the class to which object apparently belongs

## FeatureVector is a List of Strings of arbitrary size.
## It always contains a feature called "default" as its first element:
## (corresponds to -threshold in the f(x)=1, if sum_of_weights(x) - threshold >= 0
##                                        0, if sum_of_weights(x) - threshold < 0 formula
## interp. a list of features extracted from an object to be classified

## Weights is a Dictionary which maps features to weights.
## interp. weights associated with each feature

## Score is the actual sum of weights (=dot product).


## MultilabelPerceptron is MultilabelPerceptron(Collection)
## interp. a container with a perceptron targeted at each label in the collection
class MultiLabelPerceptron:

    def __init__(self, collection):
        self.perceptrons = {}
        self.collection = collection
        self.labels = collection.parts_of_speech
        for label in self.labels:
            self.perceptrons[label] = Perceptron(label)


    ## Void -> Void
    ## iterate over instances in the collection and adjust weights
    ## (until hopefully all instances are correctly classified)
    def train(self):
        macro_stats = (0.0, 0.0, 0.0, 0.0)
        STEP = 1.0
        i = 0
        j = 0
        while i < NUMBER_OF_ITERATIONS and j < 5000:
            j += 1
            print('Iteration ' + str(i))
            track_errors = 0
            for sentence in self.collection.sentences:
                for word in sentence.words:
                    word.pred_tag = self.classify(word.features)
                    if word.gold_tag != word.pred_tag:
                        track_errors += 1
                        change_weights(word.features, self.perceptrons[word.gold_tag].weights, STEP)
                        change_weights(word.features, self.perceptrons[word.pred_tag].weights, -STEP)
            new_macro_stats = evaluator.macro_averaged_stats(self.collection)
            if track_errors == 0 or macro_stats == new_macro_stats:
                print(new_macro_stats)
                break    
            elif macro_stats[2] > new_macro_stats[2]:
                STEP /= 2.0
            else:
                i += 1
                STEP = 1.0
            macro_stats = new_macro_stats
            print(macro_stats)
        
            
    ## Filename -> Void
    ## Write to a text file the label (part of speech) of each perceptron and the associated weight vector
    ## Output a list for each perceptron of the format [label, weight_vector_dictionary]
    def output_model(self, filename):
        self.train()
        outputModel = open(filename, 'w')
        print('Writing model to ' + filename)
        for label in self.perceptrons.keys():
            outputModel.write(label + '\t' + str(self.perceptrons[label].weights) + '\n' + '\n')
        outputModel.close()
        model_predictions(self.collection,filename)


    ## FeatureVector -> Prediction
    ## classify the given vector as either one or zero
    def classify(self, feature_vector):
        return max(self.perceptrons.keys(), key=lambda label: dot_product(feature_vector, self.perceptrons[label].weights))


    ## Void -> Void 
    ## Outputs labels predicted by model to checkFile
    def check_model_labels(self):
        checkFile = open ('check-model-labels.txt', 'w')
        for sentence in self.collection.sentences:
                for word in sentence.words:
                    checkFile.write(word.name + '\t' + word.pred_tag + '\n')
                checkFile.write('\n')
        checkFile.close()


## Perceptron is Perceptron(String)
## interp. a perceptron classifier targeted at the given label containing a dictionary with the weights
## for each feature
class Perceptron:

    def __init__(self, label):
        self.weights = {}
        self.label = label


## =================
## Functions:


## FeatureVector Weights -> Float
## return the sum of the weights of features stored in the feature vector
## WARNING: mutates weights (sets the weight of newly encountered features to INITIAL_WEIGHT)
def dot_product(feature_vector, weights):
    sum_of_weights = 0.0
    for feature in feature_vector:
        try:
            weights[feature]
        except KeyError:
            weights[feature] = INITIAL_WEIGHT
        sum_of_weights += weights[feature]
    return sum_of_weights


## FeatureVector Weights Float -> Weights
## increase the weights associated with features in feature_vector by step
def change_weights(feature_vector, weights, step):
    for feature in feature_vector:
        try:
            weights[feature] += step
        except KeyError:
            weights[feature] = step
            

## Collection String -> prediction file
## Reads model file and outputs predictions of that model into another file
def model_predictions(collection, modelFile):
    modelFile = open(modelFile, 'r')
    perceptrons = {}
    for line in modelFile:
        if line != '\n':
            label, weight = line.split('\t')
            weight = eval(weight)
            perceptrons[label] = weight
    modelFile.close()
    modelPredictions = open('modelPredicitions.txt', 'w')
    for sentence in collection.sentences:
        for word in sentence.words:
            word.pred_tag = max(perceptrons.keys(), key=lambda label: dot_product(word.features, perceptrons[label]))
            modelPredictions.write(word.name + '\t' + word.pred_tag + '\n')
        modelPredictions.write('\n')
    modelPredictions.close()
    evaluator.output_stats(collection, 'model-pred-stats.txt')
   # evaluator.output_pred_errors(collection, 'ouput-pred-errors.txt')
   # evaluator.print_false_negatives(collection, 'false-negatives-by-pos.txt')
    evaluator.print_fn_examples(collection, 'false-neg-ex.txt')
    print('Model prediction stats = ')
    print(evaluator.macro_averaged_stats(collection))
