#!/usr/bin/python3
# -*- coding: utf-8 -*-

## Statistical parts-of-speech tagger.
## To test the module, run: python3 -m unittest test_tagger (without the .py part)

import numpy


## =================
## Data definitions:


## Word is Word(String, String, String)
## 'features' field is a (listof String), initially empty
## interp. a wordform together with its gold standard tag, predicted tag and list of features
class Word:

    ## String String String -> Void
    def __init__(self, name, gold_tag, pred_tag):
        self.name = name
        self.gold_tag = gold_tag
        self.pred_tag = pred_tag
        self.features = []

    ## Void -> Boolean
    def classified_correctly(self):
        """Produce True if the word was classified correctly, False otherwise."""
        return self.gold_tag == self.pred_tag
    
    ## Void -> Void
    def extract_word_features(self):
        """Extract features of the word and append them to its 'features' field"""
        self.features.append('default')
        self.features.append('word=' + self.name)
        self.features.append('suffix=' + self.name[-3:])
        if self.name[-1:] == 's':
            self.features.append('last_letter=s')
        self.features.append('prefix=' + self.name[:3].lower())
        if self.name.isupper():
            self.features.append('ALL_CAPS')
        if self.name[0].isupper():
            self.features.append('first_Cap')


## Sentence is Sentence()
## 'words' field is a (listof Word), initially empty.
## interp. a sentence from a text.
class Sentence:

    # Void -> Void
    def __init__(self):
        self.words = []

    ## Word -> Void
    def add_word(self, word):
        """Add the word to sentence."""
        self.words.append(word)

    ## Void -> Void
    def extract_features(self):
        """Extract features of words in the sentence and append them to their 'features' field."""
        for i in range(len(self.words)):
            self.words[i].extract_word_features()
            if i == 0:
                self.words[i].features.extend(['pre_word_2=<s>', 'pre_word_1=<s>'])
            elif i == 1:
                self.words[i].features.extend(['pre_word_2=<s>', 'pre_word_1=' + self.words[0].name.lower()])
            else:
                self.words[i].features.extend(['pre_word_2=' + self.words[i-2].name.lower(), 'pre_word_1=' + self.words[i-1].name.lower()])
                self.words[i].features.extend(['pre_bigram=' + (self.words[i-1].name + self.words[i].name).lower()])
            try:
                self.words[i].features.append('post_word=' + self.words[i+1].name.lower())
                self.words[i].features.extend(['post_bigram=' + (self.words[i].name + self.words[i+1].name).lower()])
            except IndexError:
                self.words[i].features.append('post_word=</s>')

    ## String -> Int Int Int Int
    def generate_confusion_matrix(self, pos):
        """Given a POS tag, return a tuple with true positives, true negatives,
        false positives, false negatives for that POS.
        """
        tp, tn, fp, fn = 0.0, 0.0, 0.0, 0.0
        for word in self.words:
            if word.gold_tag == pos:
                if word.classified_correctly():
                    tp += 1
                else:
                    fn += 1
            elif word.pred_tag == pos:
                if not word.classified_correctly():
                    fp += 1
            else:
                tn += 1
        return tp, tn, fp, fn


## Collection is Collection((listof Sentences), (listof Strings))
## interp. a list of sentences (a text) together with a list of all
## part-of-speech tags (both gold and predicted) observed in sentences
class Collection:

    ## Void -> Void
    def __init__(self, sentences, parts_of_speech):
        self.sentences = sentences
        self.parts_of_speech = parts_of_speech

    ## Void -> (generator Word)
    def __iter__(self):
        """Yield all words contained in the collection."""
        for sentence in self.sentences:
            for word in sentence.words:
                yield word

    ## String -> Int Int Int Int
    def generate_confusion_matrix(self, pos):
        """Given a POS tag, return a list with true positives, true negatives,
        false positives, false negatives for that POS.
        """
        tp_tn_fp_fn = numpy.array([0.0, 0.0, 0.0, 0.0])
        for sentence in self.sentences:
            tp_tn_fp_fn += numpy.array(sentence.generate_confusion_matrix(pos))
        return tp_tn_fp_fn.tolist()


## Filename is a String.


## =================
## Functions:


## Filename Filename -> Collection
def readFiles(goldFile, predFile):
    """Given the names of the gold standard tokens file and predicted tokens file,
    return a Collection of all sentences from the files.
    """
    print('Reading file from ' + goldFile)
    listSen = []
    listPOS = []
    goldFile = open(goldFile)
    predFile = open(predFile)
    sentence = Sentence()
    i=0
    for lineGold, linePred in zip(goldFile, predFile):
        i+=1
        if lineGold != '\n':
            lineGold = lineGold.strip()
            linePred = linePred.strip()
            wordform1, tagGold = lineGold.split('\t')
            wordform2, tagPred = linePred.split('\t')
            if wordform1 == wordform2:
                word = Word(wordform1, tagGold, tagPred)
                sentence.add_word(word)
                if tagGold not in listPOS:
                    listPOS.append(tagGold)
                if tagPred not in listPOS:
                    listPOS.append(tagPred)
            else:
                print('Not matching')
               # sys.exit('Files do not match')
        else:
            listSen.append(sentence)
            sentence.extract_features()
            sentence = Sentence()
        if i%1000 == 0:
            print('Features extracted, i=' + i)
    listSen.append(sentence)
    sentence.extract_features()
    return Collection(listSen, listPOS)


## Filename Filename -> Collection
def readFiles1(filename):
    """Given the names of the gold standard tokens file and predicted tokens file,
    return a Collection of all sentences from the files.
    """
    print('Reading file from ' + filename)
    listSen = []
    listPOS = []
    filename = open(filename)
    sentence = Sentence()
    i=0
    for line in filename:
        i+=1
        if line != '\n':
            line = line.strip()
            wordform1, tag = line.split('\t')
            word = Word(wordform1, tag, '')
            sentence.add_word(word)
            if tag not in listPOS:
                listPOS.append(tag)
        else:
            listSen.append(sentence)
            sentence.extract_features()
            sentence = Sentence()
        if i%10000 == 0:
            print('Features extracted, i=' + str(i))
    listSen.append(sentence)
    sentence.extract_features()
    filename.close()
    return Collection(listSen, listPOS)
