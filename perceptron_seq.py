#!/usr/bin/python3
# -*- coding: utf-8 -*-

## A multilabel perceptron-based classifier.

import evaluator
import itertools
import numpy


## =================
## Constants:


INITIAL_WEIGHT = 0.0
NUMBER_OF_ITERATIONS = 10


## =================
## Data definitions:


## MultilabelPerceptron's Prediction is a String.
## interp. label of the class to which object apparently belongs

## FeatureVector is a List of Strings of arbitrary size.
## It always contains a feature called "default" as its first element:
## (corresponds to -threshold in the f(x)=1, if sum_of_weights(x) - threshold >= 0
##                                        0, if sum_of_weights(x) - threshold < 0 formula
## interp. a list of features extracted from an object to be classified

## Weights is a Dictionary which maps features to weights.
## interp. weights associated with each feature

## Score is the actual sum of weights (=dot product).


## MultilabelPerceptron is MultilabelPerceptron(Collection)
## interp. a container with a perceptron targeted at each label in the collection
class MultiLabelPerceptron:

    def __init__(self, collection):
        self.perceptrons = {}
        self.collection = collection
        self.labels = []
        self.pair_labels = []
        self.grid = []

    def add_perceptrons(self):
        for label in self.pair_labels:
            self.perceptrons[label] = Perceptron(label)

    ## Void -> Void
    ## iterate over instances in the collection and adjust weights
    ## (until hopefully all instances are correctly classified)
    def train(self):
        self.labels = self.collection.parts_of_speech
        self.pair_labels = pairlabels(self.labels)
        self.add_perceptrons()
        macro_stats = (0.0, 0.0, 0.0, 0.0)
        STEP = 1.0
        i, j = 0, 0
        while i < NUMBER_OF_ITERATIONS and j < 500:
            j += 1
            print('Iteration ' + str(i))
            track_errors = 0
            for sentence in self.collection.sentences:
                self.grid = numpy.zeros((len(sentence.words), len(self.labels)))
                first_label = '|O'
                k = 0
                while k < len(sentence.words)-1 and k < 500:
                    pair_vector = sentence.words[k].features + sentence.words[k+1].features
                    sentence.words[k+1].pred_tag = self.classify_seq(pair_vector, first_label, k)
                    if sentence.words[k+1].pred_tag != sentence.words[k+1].gold_tag:
                        track_errors += 1
                        change_weights(pair_vector, self.perceptrons[(sentence.words[k].gold_tag, sentence.words[k+1].gold_tag)].weights, STEP)
                        change_weights(pair_vector, self.perceptrons[(first_label, sentence.words[k+1].pred_tag)].weights, -STEP)
                    first_label = sentence.words[k+1].pred_tag
                    k += 1
            new_macro_stats = evaluator.calculate_prfa(self.collection.sequential_confusion_matrix())
            if track_errors == 0 or macro_stats == new_macro_stats:
                print(new_macro_stats)
                break    
            elif macro_stats[2] > new_macro_stats[2]:
                STEP /= 2.0
            else:
                i += 1
                STEP = 1.0
            macro_stats = new_macro_stats
            print(macro_stats)
        self.check_model_labels()
        
            
    ## Filename -> Void
    ## Write to a text file the label (part of speech) of each perceptron and the associated weight vector
    ## Output a list for each perceptron of the format [label, weight_vector_dictionary]
    def output_model(self, filename):
        self.train()
        outputModel = open(filename, 'w')
        print('Writing model to ' + filename)
        outputModel.write('Labels' + '\t' + str(self.labels) + '\n' + '\n')
        outputModel.write('Pair_labels' + '\t' + str(self.pair_labels) + '\n' + '\n')
        i=0
        for label in self.perceptrons.keys():
            i+=1
            outputModel.write(str(label) + '\t' + str(self.perceptrons[label].weights) + '\n' + '\n')
        outputModel.close()
#        model_predictions(self.collection, filename)



    ## FeatureVector -> Prediction
    ## classify the given vector as either one or zero
    def classify(self, feature_vector):
        return max(self.perceptrons.keys(), key=lambda label: dot_product(feature_vector, self.perceptrons[label].weights))


    def classify_seq(self, feature_vector, first_label, word_k):
        for double_label in starting_with_label(first_label, self.pair_labels):          
            self.grid[word_k, self.labels.index(double_label[1])] = dot_product(feature_vector, self.perceptrons[double_label].weights)
        scores_word_k = self.grid[word_k]
        return self.labels[scores_word_k.argmax()]
        

    ## Void -> Void 
    ## Outputs labels predicted by model to checkFile
    def check_model_labels(self):
        checkFile = open ('check-model-labels.txt', 'w')
        for sentence in self.collection.sentences:
                for word in sentence.words[1:]:
                    checkFile.write(word.name + '\t' + word.pred_tag + '\n')
                checkFile.write('\n')
        checkFile.close()
        


## Perceptron is Perceptron(String)
## interp. a perceptron classifier targeted at the given label containing a dictionary with the weights
## for each feature
class Perceptron:

    def __init__(self, label):
        self.weights = {}
        self.label = label
        

## =================
## Functions:


## FeatureVector Weights -> Float
## return the sum of the weights of features stored in the feature vector
## WARNING: mutates weights (sets the weight of newly encountered features to INITIAL_WEIGHT)
def dot_product(feature_vector, weights):
    sum_of_weights = 0.0
    for feature in feature_vector:
        try:
            weights[feature]
        except KeyError:
            weights[feature] = INITIAL_WEIGHT
        sum_of_weights += weights[feature]
    return sum_of_weights


## FeatureVector Weights Float -> Weights
## increase the weights associated with features in feature_vector by step
def change_weights(feature_vector, weights, step):
    for feature in feature_vector:
        try:
            weights[feature] += step
        except KeyError:
            weights[feature] = step
            

## Collection String -> prediction file
## Reads model file and outputs predictions of that model into another file
def model_predictions(collection, modelFile, modelPredFile):
    modelFile = open(modelFile, 'r')
    modelMLP = MultiLabelPerceptron(collection)
    for line in modelFile:
        if line != '\n':
            first, second = line.split('\t')
            if first == 'Labels':
                modelMLP.labels = eval(second)
            elif first == 'Pair_labels':
                modelMLP.pair_labels = eval(second)
                modelMLP.add_perceptrons()
            else:
                label, weight = eval(first), eval(second)
                modelMLP.perceptrons[label].weights = weight
    modelFile.close()
    modelPredictions = open(modelPredFile, 'w')
    for sentence in modelMLP.collection.sentences:
        modelMLP.grid = numpy.zeros((len(sentence.words), len(modelMLP.labels)))
        first_label = '|O'
        k = 0
        while k < len(sentence.words)-1 and k < 5000:
            pair_vector = sentence.words[k].features + sentence.words[k+1].features
            sentence.words[k+1].pred_tag = modelMLP.classify_seq(pair_vector, first_label, k)  
            if sentence.words[k+1].pred_tag[1] == 'I' and first_label[1] == 'O':
                sentence.words[k+1].pred_tag = '|B' + sentence.words[k+1].pred_tag[2:]
            modelPredictions.write(sentence.words[k+1].name + '\t' + sentence.words[k+1].pred_tag + '\n')
            first_label = sentence.words[k+1].pred_tag
            k += 1   
        modelPredictions.write('\n')
    modelPredictions.close()
#    print('Model prediction stats = ')
#    print(evaluator.macro_averaged_stats(modelMLP.collection))
    return modelMLP
    
## (listof String) -> (listof Tuple)
## Produce all possible bigrams of elements in the list
## ASSUME: given list containts at least 2 elements
def pairlabels(l):
    label_list = []
    for item in l:
        label_list.append((item, item))
    label_list.extend(list(itertools.permutations(l, 2)))
    return label_list

def test_pairlabels():
    assert pairlabels(["a", "b"]) == [("a", "b"), ("b", "a")]

def starting_with_label(first_label, label_list):        
    return [double_label for double_label in label_list if double_label[0] == first_label]

    