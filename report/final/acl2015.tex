% NOTE: How to count number of tokens or sentences:
% 1. (wc -l test-nolabels.col) for total number of lines
% 2. (grep '^[ \t]*$' test-nolabels.col | wc -l) for counting sentences
% 3. (1-2) for number of tokens
% just fyi

% NOTE: how to order named entities in 'nes-' files by frequency:
% sort nes-2015-07-06.txt | uniq -c | sort -nr | head -n 10
% (to get 10 most frequent)
% sort nes-2015-07-06.txt | uniq -c | sort -nr | tail -n 10
% (to get 10 least frequent)

\documentclass[11pt]{article}
\usepackage{acl2015}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}
\usepackage[normalem]{ulem}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{algpseudocode}

\title{Multi-Label Perceptron for Part of Speech and Named Entity Tagging: \\ Identifying Errors and Real-World Trends}

\author{Bethany Lochbihler\qquad Ilnar Salimzianov \\
        Institute for Natural Language Processing \\
        University of Stuttgart \\
        \{bethany.lochbihler, ilnar.salimzianov\}@ims.uni-stuttgart.de}

\makeatletter
\def\@biblabel#1{}
\makeatother

\begin{document}

\maketitle

\begin{abstract}

This paper discusses two automatic tagging systems developed to predict part of speech labels of individual word tokens, and to identify named entity phrases. We use a multi-label perceptron for supervised learning and implement token-based and sequential label models for the tagging tasks. We apply our sequential system to identify named entity phrases in raw crawled news data, and we evaluate our findings by frequency of predicted entities from different dates. Our experiments show improved part-of-speech tagging with added features after error analysis, but produce worse results when the sequential model is applied. However, for the named entity recognition task, the sequential model performs much better than the token-based model and the most frequent entities extracted from real-world data appear to reflect major news topics for the dates considered. 

\end{abstract}

\section{Introduction}

In this work we developed a statistical word/entity tagger applied to a Part of Speech (POS) tagging task and a Named Entity Recognition (NER) task. Our system performs supervised training with a Multi-label Perceptron, ranging over perceptrons for each POS label (to predict a label for each token) or sequences (bigrams) of NER labels (to detect phrases). We applied the sequential version of the tagging system to process crawled news data \cite{Ortega and Yu:15} and compared the most frequent detected NER phrases from several different dates. The combination of the sequential system and real-time crawled data can be used to identify trending topics and the relative impact of certain news topics at a given point in time.

\section{Methods and Data}

The models for both the POS and NE tagging tasks are developed via supervised machine learning with a perceptron linear classifier. Training data for POS tagging is taken from the English section of the OntoNotes corpus \cite{Weischedel et al:13}, and for the NER task from the CoNLL 2003 shared task corpus \cite{Tjong Kim Sang and De Meulder:03}.

The perceptron (\cite{Rosenblatt:58}, see also \cite{Winston:92}; \cite{Russel and Norvig:10}, for example) computes a weight vector that classifies a token as part of the perceptron class or as excluded by it, depending on the featural makeup of that token. The algorithm begins with a default weight vector, for instance a zero vector, and extracts feature vectors for each token (e.g. the surface form and previous word, see section 3.1). The dot product of the weight vector and the feature vector for each token determines the prediction $P$ for that token (i.e. whether or not the token is in the class):

$$P = \begin{cases} 1 &\mbox{if } \sum_{i} w_i x_i > T \\ 0 &\mbox{otherwise }\end{cases}$$

where $w_i$ is the perceptron weight value, $x_i$ is the corresponding feature value, and their product is summed over all the features for a token. This dot product score is compared to a threshold parameter $T$, to determine if the token is predicted to be within the perceptron class or not.
%TODO: The sentence below still makes no sense to me because I don't know why this needs to be mentioned here. Either add in how it connects to the previous discussion or remove. (Do we adjust thresholds for our system? I thought that was not relevant for the multi-label perceptron....)
	% We do not adjust it, we make the perceptron learn it.
    % If we say above that "the dot product is compared to a threshold parameter T",
    % the question arises -- how do we know the threshold?
    % If we just compare the dot product to zero, it means that the separating (hyper)plane
    % will always go through the origin.
    % To avoid that, we add one more feature to our feature vector (x_0, "an extra
    % input" -- which is always = 1), one more weight to our weight vector (w_0, "an extra
    % weight" -- so that at the end of training w_0 = -threshold), and set the threshold to be zero.
    % See slide 11 of 03-multi-class-perceptron.pdf
    % 
    % Canonical formula of a perceptron is either w*x > theta or w*x + b > 0
    % I am pretty sure Dr. Klinger will pay to attention to this, that's why I am so
    % obstinate about keeping the sentence (in this form or another).
A non-zero threshold can be converted into the combination of an extra input, an extra weight and a zero threshold\cite[p. 472]{Winston:92}.

If the prediction for a token is incorrect, then the weight vector is adjusted to better reflect a linear division between classes. The adjustment is applied in a step size, for example if $STEP=1$ then the non-zero values of the feature vector are added or subtracted to the perceptron weight:

\begin{algorithmic}
\If {$prediction = 0$ and $expected = 1$}
    \State $\vec{w}\gets \vec{w} + STEP \times \vec{x}$
\EndIf
\If {$prediction = 1$ and $expected = 0$}
    \State $\vec{w}\gets \vec{w} - STEP \times \vec{x}$
\EndIf
\end{algorithmic}

It is assumed that the classes being evaluated are linearly separable, which is not necessarily the case for natural language data, but the distinction is considered to be negligible for our system. 

\subsection{Multi-label Perceptron for Token Tagging}

A binary perceptron can only distinguish between two classes: a token is either part of the class, or it is not. Our POS tagger must distinguish between 48 possible tags, so we implement a Multi-label Perceptron \cite{Crammer and Singer:03} that calculates a weight vector for each POS tag and classifies a token depending on which POS perceptron returns the highest score ($p$ ranges over POS tags):

$$P = \arg\max_p \sum_{i} w_i^p x_i$$

When an incorrect prediction is made the weights of the predicted tag are decreased by a step (default step size = 1) for the feature vector of that token, and the weights for the correct tag are increased by a step:

\begin{algorithmic}
\If {$prediction \neq expected$}
    \State $\vec{w_e}\gets \vec{w_e} + STEP \times \vec{x}$
    \State $\vec{w_p}\gets \vec{w_p} - STEP \times \vec{x}$
\EndIf
\end{algorithmic}

The process is repeated for each iteration over the training data (default iterations = 10). 

Our system monitors performance of the adjusted weight vectors to ensure that predictions are improving in each iteration. First, the F1-score of the current iteration is compared with the F1-score of the previous iteration to ensure that it is increasing. A decrease in the F1-score after updating weights can indicate that the adjustment was too large and has overshot the (higher-dimensional) plane separating the classes. In this case we cut the step size in half (e.g. 1 to 0.5, 0.5 to 0.25) and re-run the iteration until the F1-score improves:

\begin{algorithmic}
\State $Iteration \gets 0$
\State $STEP \gets 1$
\While {$Iteration < IterationsLimit$}
  \If {$F1_n-_1 > F1_n$}
    \State $STEP \gets STEP \times 0.5$
  \Else
      \State $Iteration \gets Iteration + 1$
      \State $STEP \gets 1$
  \EndIf
\EndWhile
\end{algorithmic}

Second, we compare the macro-averaged statistics between iterations to determine if they continue to change or if they have become static, in which case our system cannot make better predictions and the training algorithm is terminated.

\subsection{Multi-label Perceptron for Named Entity Recognition}

The NER task also uses a Multi-label Perceptron in order to distinguish between words within a named entity phrase (e.g. organizations, locations, people), and words which do not belong to a named entity. We implement a sequential model to detect named entity phrases with perceptrons ranging over pairs of BIO (beginning, inside, outside) labels (e.g. [O, B-ORG]) that make predictions on the second label in the pair given the first label. Each sentence begins with a dummy 'start' token with label 'O'. Only label pairs that begin with the given first label are considered ($L$ is the predicted label, superscripts indicate pair labels/words):

$$L_n = \arg\max_l \sum_{i} w_i^{[L_{n-1},\ l]}  x_i^{[n-1,\ n]}$$

where $l$ ranges over all the single-labels (e.g. O, B-ORG, I-ORG etc.\footnote{Some sequences like [O, I-ORG] should not be predicted, since an I label must be preceded by a B label. We allow the system to make such predictions but post-process them to convert [O, I] into [O, B].})

The sequential model is meant to improve predictions for the NER task since it is concerned with entire, continuous phrases, which must be detected as a unit. Classification by individual tokens
does not sufficiently model the phrase internal relationships between tokens identifying a named entity. 

\section{Experiments}

\subsection{Part-of-Speech Tagging}

Our POS tagger is trained and tested on the English data from the OntoNotes corpus.

Training data consisted of 30060 sentences, development data of 1083 sentences, and testing data of 1382 sentences.

In training and development, our system performs supervised learning from tokenized gold standard labeled data in order to train the multi-label perceptron weights, and to test the performance on the development data. In the development of our POS tagging system we conducted three part-of-speech tagging experiments:

\begin{enumerate}
\item Baseline word- and sentence-level features (8 in total),
\item Effect of extended features (bigrams, final-s) on common errors,
\item Implementing the sequential model developed for the NER task on POS tagging.
\end{enumerate}

The final set of feature templates we used for POS tagging are shown in Table \ref{tab:features}.

\subsubsection{Experiment 1: POS Tagging with Baseline Features}

For the first experiment we tested our POS tagging system with the word-level and sentence-level features in Table \ref{tab:features}. Results for the predictions made on the development data are shown in table \ref{tab:initial_results} (10 iterations, in all experiments), and the macro-average F1-score was 89.06\%

\begin{table}
\centering
\begin{tabular}{l|p{2cm}|l}
Word-level & Sentence-level & Extended \\\hline
surface form & prev-word 1 & prev-word+word \\
3-letter prefix & prev-word 2 & word+post-word \\
3-letter suffix & post-word 1 & final-s? \\
ALL-CAPS? &  &  \\
First-Cap? &  &  
\end{tabular}
\caption{\label{tab:features}Features used for POS tagging}
\end{table}

\begin{table}
\centering
\begin{tabular}{l|r|r|r|r}
Avg.       & Precision & Recall & F1 & Accuracy \\\hline
Macro      & 90.18 & 89.24 & 89.06 & 99.78 \\
Micro      & 95.15 & 95.15 & 95.15 & 99.78
\end{tabular}
\caption{\label{tab:initial_results}Results for experiment 1 (in \%, trained on train.col, tested on dev.col)}
\end{table}

\subsubsection{Error Analysis}

In order to identify problems in our POS tagger we extracted POS tags that had the largest proportion of false negatives. We analysed the four groups of errors below, with examples of each given in Table \ref{tab:false_negatives}:

\begin{itemize}
\item Plural proper nouns (NNPS) tagged as singular proper/common nouns (NNP/NN)
\item Superlative adverbs (RBS) often tagged as superlative adjectives (JJS).
\item Confusion between adjectives (JJR) and adverbs (RBR), and vice versa.
\item All four instances of interjections (UH) from development data were predicted wrongly. This has a large impact on macro-averaged results since it represents an entire POS tag category. 
\end{itemize}

\begin{table}
\centering
\begin{tabular}{l|l|l}
Problem POS & Example & Predicted tag \\\hline
NNPS & Farmers & NNP \\
& Farmers & NN \\\hline
RBS & most & JJS \\
& fastest & JJS \\\hline
JJR & more & RBR \\
& early & RBR \\\hline
RBR & more & JJR \\
& lower & JJR
\end{tabular}
\caption{\label{tab:false_negatives} Examples of large proportion false negatives for POS tagger}
\end{table}

\subsubsection{Experiment 2: POS Tagging with Extended Features}

Our second experiment aims to improve the performance of the POS tagging system by adding features that should linguistically differentiate between some of the wrongly predicted tags discussed in Section 3.1.2. We incrementally added the extended features in Table \ref{tab:features}, and evaluated their impact on predictions for the development data. The added features are as follows:

\begin{itemize}
\item Two bigram features (current word plus previous or following word) to differentiate between adjectives and adverbs (i.e. adjectives often precede nouns, adverbs precede verbs/ adjectives).
\item A final-s feature to target the regular plural marking in English and distinguish between plural proper nouns and singular nouns. 
\end{itemize}

Figure \ref{fig:extended_feat_by_pos} illustrates the changes in proportion of false negatives by POS with different feature sets. Predictions for UH remain at 0\%; the final-s feature slightly improves the NNPS prediction, but the bigram features worsen predictions; bigrams slightly improve predictions for RBS; both bigram and final-s have a positive impact on JJR, but worsen predictions for RBR. Results for the development data with the extended features are shown in Table\ref{tab:contribution_of_new_features}.

	Overall, the extended feature set marginally improves the performance of the POS tagger system, and the features do affect the specific POS tags as expected, except for RBR. For example, the final-s feature is meant to differentiate between NNPS and singular noun tags, and this is reflected in the lower proportion of false negatives for that category. Further experimentation with various feature sets is required to identify which features will successfully and significantly distinguish between frequently confused POS tags without worsening results for other categories. 

\begin{figure*}
\centering
\includegraphics[scale=0.8]{FNposgraph.pdf}
\caption{\label{fig:extended_feat_by_pos} Proportion of false negatives by POS for extended feature sets}
\end{figure*}

\begin{table}
\centering
\begin{tabular}{l|r|r|r|p{1cm}}
Features  & Precision & Recall & F1  & Accu-racy \\\hline
Original  & 90.18 & 89.24 & 89.06 & 99.78 \\          
+Final-s? & 90.60    & 89.13   & 89.33 & 99.80 \\
+Bigrams  & 91.32    & 88.36   & 89.06 & 99.81 \\
All  & 91.54    & 88.96   & 89.71 & 99.81 \\
\end{tabular}
\caption{\label{tab:contribution_of_new_features}Macro-averaged results with extended feature sets (in \%).}
\end{table}

We tested a POS tagger model that extracted all features in Table \ref{tab:features} and was trained on the combined training and development collections. This model was tested on a set of blind data and independently evaluated. We obtained the results in Table \ref{tab:nolabels_results}. 
\begin{table}
\centering
\begin{tabular}{l|l|l}
Precision & Recall   & F1 \\\hline
90.2    & 91.4   & 89.7 \\
\end{tabular}
\caption{\label{tab:nolabels_results}Macro-averaged results for unlabeled test data (in \%).}
\end{table}

\subsubsection{Experiment 3: POS Tagging Using the Sequential Model}

Experiment 3 revisits the POS tagger using the sequential model developed for the NER task (further discussed below), which predicts a tag for a token given the prediction for the previous token in a sentence.The results for the development data are given in Table \ref{tab:pos_sequential}. The F1-score is lower than the results from the token model (see Table \ref{tab:contribution_of_new_features}). This result might indicate that POS tagging is not best modeled by a sequential task that limits the predictions for a word based on the prediction for the previous word. Future work could perform error analysis to uncover the cause of this decrease in performance. 

\begin{table}
\centering
\begin{tabular}{l|l|l}
Precision & Recall   & F1 \\\hline
85.81    & 83.12   & 83.78\\
\end{tabular}
\caption{\label{tab:pos_sequential}Results for POS tagging task in sequential model (in \%).}
\end{table}

\subsection{Named Entity Recognition}

Building on the multi-label perceptron framework of our POS tagger, we extended the system to the Name Entity Recognition task. We trained and tested the NER system on the CoNLL English BIO data, and further applied our NE tagger to data crawled from Google News on different dates. 

The model of the NE tagger is the same as the POS tagger, except instead of only considering individual tokens the learning algorithm considers pairs of adjacent words. Given the label assigned to the first word in the pair, a prediction is made on the pair label and the result is assigned to the predicted tag of the second word in the pair. Therefore, the prediction for a word is directly dependent on the preceding word, which more closely models the phrase recognition task of NER. 
We performed two experiments for the NER task: 
\begin{enumerate}
\item Comparison of sequential and non-sequential model on test data.
\item Evaluating frequency of predicted named entities from crawled news data on different dates. 
\end{enumerate}

\subsubsection{Experiment 4: NER on CoNLL-2003 Shared Task Data}
Experiment 4 compared the results of the sequential label multi-label perceptron on named entity recognition to the non-sequential model developed for POS tagging. The system used the features in Table \ref{tab:features}, and training data was in the BIO format, so that elements are tagged as beginning, inside, or outside a named entity phrase and further categorized as an organization, person, location, or miscellaneous.

The results of experiment 4 are given in Table \ref{tab:NER_results}, comparing results when evaluated by individual token and when full NE phrases are detected. The non-sequential model gives an F1-score of 68.98\% for exact phrase labeling, which is significantly lower than the sequential model with an F1-score of 93.49\%. 

\begin{table}
\centering
\begin{tabular}{l|r|r}
 & Token F1 & Phrase F1    \\\hline
Non-sequential & 75.89 & 68.98  \\          
Sequential & 93.19    & 93.48   \\
\end{tabular}
\caption{\label{tab:NER_results}Comparison of non-/sequential results for NER task (in \%, trained on train.iob + dev.iob, tested on test.iob).}
\end{table}

\subsubsection{Experiment 5: NER on Crawled News Data}

The last experiment uses our sequential tagger to label named entities in crawled news data. Using the news crawler developed by Ortega and Yu, we collected data from a span of different dates and determined which named entity phrases were most frequent each day. The model for the news data was trained on the combined training, development and test CoNLL-2003 data.
Table \ref{tab:nes_0108} shows the ten most frequently predicted named entities for August 1, 2015.

% sort nes-2015-08-01.txt | uniq -c | sort -nr | head -n 10
%                    or (for all days)
% for file in *.txt; do echo "10 most freq. nes in $file"; sort $file | \
% uniq -c | sort -nr | head -n 10; done
\begin{table}
\centering
\begin{tabular}{l|l}
Freq./NE \hfill Category         & Freq./NE \hfill Category \\\hline
293  Clinton \hfill PER         & 108  U.S \hfill LOC \\
254  U.S. \hfill LOC            & 107  Turkey \hfill LOC \\
157  Hillary Clinton \hfill PER & 105  Turkish \hfill MISC \\
147  China \hfill LOC           & 97   French \hfill MISC \\
144  United States \hfill LOC   & 91   Zimbabwe \hfill LOC \\
\end{tabular}
\caption{\label{tab:nes_0108}10 most frequent named entities for August 1, 2015.}
\end{table}

Table \ref{tab:nes_by_date2} shows 5 most frequent named entities for each category for the six days between 06.07-02.08.2015. There are several phrases that are found across several dates, such as 'Obama', 'BBC' and 'Greece'. 

% NOTE: the following two tables are identical (one is transpose of the other), which
%            means that one of them should be deleted.
%            I like the second one more, where NER categories are rows.
%            Giving 10 examples for each category seemed like an overkill.
% I don't know how these information can be visualized, but in case you want to plot it
% somehow, here is the same data in tsv format: http://pastebin.com/wV5UCBdR

%--> I think the second table is better. We can't visualize it, there aren't enough shared terms

\begin{table*}
\small
\centering
\setlength{\tabcolsep}{2.7pt}
\begin{tabular}{l|l|l|l|l|l|l}
Category & 06.07 & 28.07 & 29.07 & 30.07 & 01.08 & 02.08 \\
\hline
PER  & 186 Obama          & 182 Obama   & 77 Cosby    & 113 Obama   & 293 Clinton         & 166 Trump \\
     & 176 Clinton        & 153 Merkel  & 46 Brady    & 81 Clinton  & 157 Hillary Clinton & 102 Clinton \\
     & 116 Michelle Obama & 103 George  & 35 Trump    & 59 Omar     & 81 Obama            & 97 Obama \\
     & 106 George         & 97 Clinton  & 35 Cheney   & 58 Trump    & 53 Stewart          & 80 Paul \\
     & 87 Trump           & 93 Van Sant & 33 Mitchell & 54 Brady    & 50 Mullah Omar      & 54 Roberts \\
\hline
ORG  & 380 EU             & 369 EU      & 54 BBC      & 98 Congress & 73 BBC              & 52 Ebola \\
     & 254 US             & 277 US      & 39 CNN      & 63 Samsung  & 61 Reuters          & 49 Arsenal \\
     & 194 BBC Monitoring & 191 BBC Monitoring & 29 NFL & 62 BBC & 60 PKK & 44 Chelsea \\
     & 130 European Union & 114 ABC & 26 Ford & 47 ABC News & 59 Congress & 33 PKK \\
     & 119 UN & 113 UN & 19 Microsoft & 43 Senate & 55 Celtic & 31 EU \\
\hline
LOC  & 802 Greece & 571 Germany & 76 UK & 193 U.S. & 254 U.S. & 239 U.S. \\
     & 599 Germany & 506 Europe & 47 Turkey & 94 UK & 147 China & 213 Iraq \\
     & 535 Europe & 303 China & 43 US & 91 United States & 144 United States & 87 United States \\
     & 447 U.S. & 291 Russia & 32 U.S. & 91 China & 108 U.S & 73 Zimbabwe \\
     & 302 China & 290 Greece & 32 London & 86 U.S & 107 Turkey & 63 U.S \\
\hline
MISC & 595 German & 467 German  & 52 French  & 80 American  & 105 Turkish  & 104 Republican \\ 
     & 541 Greek  & 307 European  & 35 British  & 74 Olympic  & 97 French  & 68 American \\ 
     & 421 European  & 267 Russian  & 23 Iran  & 74 Chinese  & 76 Taliban  & 58 Democratic \\ 
     & 354 British  & 241 British  & 22 Windows  & 69 Taliban  & 74 Chinese  & 57 Midlothian \\ 
     & 301 euro  & 214 Greek  & 22 Republicans  & 65 Republican  & 71 American  & 55 French \\ 
\end{tabular}
\caption{\label{tab:nes_by_date2}Most frequent named entities for each category by date.}
\end{table*}

As an example of the frequency changes over time, figure \ref{fig:turkey} depicts normalized frequency (number of occurrences of the NE / total number of tokens in the corpus) of 'Turkey' with label 'location' over time.  

\begin{figure}
\centering
\includegraphics[scale=0.43]{turkey.png}
\caption{\label{fig:turkey} Normalized frequency of "Turkey LOC" named entity over time ($10^{-6}$).}
\end{figure}

% How did I calculate the normalized frequency:
% # of lines (~words) in the corpus) : # of times Turkey LOC occurs
%    (wc -l)                             (sort nes-2015-07-06.txt |uniq -c |
%                                           sort -nr | grep Turkey)
%  1466200 2015-07-06.iob            : 115  Turkey	LOC
%  1049117 2015-07-28.iob            : 268  Turkey	LOC
%  143339 2015-07-29.iob             : 47  Turkey	LOC
%  406668 2015-07-30.iob             : 10  Turkey	LOC
%  481453 2015-08-01.iob             : 107  Turkey	LOC
%  265614 2015-08-02.iob             : 25  Turkey	LOC 
%  508895 2015-08-06.iob             : 26  Turkey	LOC
%
% e.g. for 06.07.2015: 115 / 1466200 * 1000000 = 78
%
% 06.07 78
% 28.07 255
% 29.07 328 
% 30.07 25
% 01.08 222
% 02.08 94
% 06.08 51

\section{General Discussion}

% SEE BELOW: There were two commented out sentences in the general discussion, was there a reason for that? I undid the commenting out.
	% I had commented them out because they didn't sound right to me.
    % Actually, I was surprised that results for POS-tagging using the sequential model
    % were worse than results obtained using the token-based model.
    %
    % POS-tagging is considered to be a sequence labeling/sequence tagging problem (see e.g.
    % https://en.wikipedia.org/wiki/Sequence_labeling), and assigning tags for entire sequences
    % is expected to produce better results than going token by token (POS tags do influence
    % each other, and a noun tag after an article is more likely than after an adverb).
    %
    % I'm not sure why we got what we got (maybe there is some error propagation happening,
    % which we don't really handle -- we basically take currently best decision for each token
    % and don't consider other alternatives -- or sequences we consider (bigrams) are not long
    % enough), anyway, I am pretty sure it's not because of the nature of the POS-tagging task,
    % hence I didn't want to go into explaining it :)

%--> Ok, I understand your hesitation, but we have to report the results we actually get, even if we can't explain them. I think it does make sense -- NER you only have to predict like [B-ORG, I-ORG]. In order for the sequential task to work for POS is the predict the entire sentence in sequence (i.e. there is no O label). So I think we should include this. We don't have to say that's exactly why it happens, but we can't exclude it from discussion. 
		% Agreed.
We interpret the results as indicating that each of the two taggers is better suited for a particular task, and that further improvement of results could be obtained by more experimentation with feature engineering.

The POS tagger is a token-based task, and although the predictions for a specific token can rely on the adjacent words (represented in prev/post-word and bigram features) it is not actually a phrase recognition task. For instance, there is no 'O'/outside label since every token in a sentence must receive a label, and the sequential model restricts predictions for a token depending on the prediction for the previous word. It may be that the sequences of POS tags are much less predictable than those for NE phrases (e.g. of the form [B-ORG, I-ORG, I-ORG]) and that the features extracted from the token and adjacent words are more informative than their sequence. At this point we cannot conclude exactly why the results vary for the POS tagging task for the token and sequential models, but further error analysis could shed light onto these results. 

On the other hand, the goal of named entity recognition tagging is to identify full phrases to the exclusion of all other words, and so the sequential aspect is well-suited to this task.
Our experiments show that although token tagging can identify more than half of the NE phrases in the development data, the sequential tagger provides vastly better results. 

Further, the most frequent NE phrases identified in the raw crawled data are as expected: the type tags are correct (e.g. Person, Location), there are several shared most frequent NEs across dates (e.g. 'Clinton', 'United States'), and these NEs reflect current affairs (e.g. U.S. Presidential campaigns). We expect that there will be shared, highly frequent phrases on adjacent days, as illustrated in figure \ref{fig:turkey} and table \ref{tab:nes_by_date2}, since news topics typically cycle through a few days. We also assume that the frequency results are very dependent on which news sources are crawled, particularly from what region (e.g. US or UK) and in which language. 

	The multi-label perceptron model proves to be effective for these types of tasks, although its effectiveness varies depending on the specific task, the features extracted, and the prediction model (i.e. by token or sequential). 

\section{Conclusion}

The task of token or phrase recognition has important applications for analyzing text based information, like news or other published material. 

POS tagging can be fed into other natural language processing systems, such as dependency parsing, to automatically determine the structure of the data. Named entity recognition, as shown in the the above sections, has direct applications in extracting trends in real-time data. This kind of information could be coupled with data on what NE phrases from news headlines are most often viewed by users to determine what trends are particularly popular. 

Future research for the NER task could look into feature engineering to uncover which added features might be beneficial, as well as applying other models, such as variations on Conditional Random Fields to compare to the Multi-Label Perceptron, and other prediction structures, like the Viterbi algorithm to further evaluate the sequences predicted.

\begin{thebibliography}{}

\bibitem[\protect\citename{Crammer and Singer}2003]{Crammer and Singer:03}
Koby Crammer and Yoram Singer.
\newblock 2003.
\newblock {\em Ultraconservative online algorithms for multiclass problems}.
\newblock The Journal of Machine Learning Research 3, 951-991.

\bibitem[\protect\citename{Ortega and Yu}2015]{Ortega and Yu:15}
Daniel Ortega and Xiang Yu.
\newblock 2015.
\newblock {\em Perceptron-based Sequential Model for Classification Tasks}.
\newblock Team Lab 2015 project reports. Institute for Natural Language Processing, University of Stuttgart.

\bibitem[\protect\citename{Russel and Norvig}2010]{Russel and Norvig:10}
Stuart Russel and Peter Norvig.
\newblock 2010.
\newblock {\em Artificial Intelligence: A Modern Approach (3rd edition)}.
\newblock Upper Saddle River, NJ: Prentice Hall., 727-731.

\bibitem[\protect\citename{Rosenblatt}1958]{Rosenblatt:58}
Frank Rosenblatt.
\newblock 1958.
\newblock {\em The Perceptron: A probabilistic model for information storage and organization in the brain}.
\newblock Psychological Review, 65(6), 386-408.

\bibitem[\protect\citename{Tjong Kim Sang and De Meulder}2003]{Tjong Kim Sang and De Meulder:03}
Erik~F. Tjong Kim Sang and Fien De~Meulder.
\newblock 2003.
\newblock {\em Introduction to the CoNLL-2003 shared task: language-independent named entity recognition}.
\newblock In Proceedings of the seventh conference on Natural language learning at HLT-NAACL 2003 - Volume 4 (CONLL '03), Vol. 4. Association for Computational Linguistics, Stroudsburg, PA, USA, 142-147.

\bibitem[\protect\citename{Weischedel et al.}2013]{Weischedel et al:13}
Ralph Weischedel, Martha Palmer, Mitchell Marcus,
Eduard Hovy, Sameer Pradhan, Lance Ramshaw, Nianwen Xue, Ann Taylor,
Jeff Kaufman, Michelle Franchini, Mohammed El-Bachouti, Robert Belvin,
Ann Houston.
\newblock 2013.
\newblock {\em OntoNotes Release 5.0}.
\newblock \href{https://catalog.ldc.upenn.edu/LDC2013T19}{https://catalog.ldc.upenn.edu/LDC2013T19}.

\bibitem[\protect\citename{Winston}1992]{Winston:92}
Patrick Henry Winston.
\newblock 1992.
\newblock {\em Artificial Intelligence}.
\newblock Reading, Mass: Addison-Wesley Pub. Co., 471-489.

\end{thebibliography}

\end{document}
