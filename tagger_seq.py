#!/usr/bin/python3
# -*- coding: utf-8 -*-

## Statistical parts-of-speech tagger.
## To test the module, run: python3 -m unittest test_tagger (without the .py part)

import numpy


## =================
## Data definitions:


## Word is Word(String, String, String)
## interp. a wordform together with its gold standard tag, predicted tag and list of features
## 'features' field is a ListOfString, initially empty
class Word:

    ## Void -> Void
    def __init__(self, name, gold_tag, pred_tag):

        self.name = name
        self.gold_tag = gold_tag
        self.pred_tag = pred_tag
        self.features = []

    ## Void -> (generator Word)
    def __iter__(self):
        """Yield all words contained in the collection."""
        for sentence in self.sentences:
            for word in sentence.words:
                yield word

    ## Void -> Boolean
    ## produce True if gold and predicted tags match, False otherwise
    def classified_correctly(self):
        return self.gold_tag == self.pred_tag
    
    ## Void -> Void
    ## extract features of the word and append them to its 'features' field
    def extract_word_features(self):
        self.features.append('default')
        self.features.append('word=' + self.name)
        self.features.append('suffix=' + self.name[-3:])
        if self.name[-1:] == 's':
            self.features.append('last_letter=s')
        self.features.append('prefix=' + self.name[:3].lower())
        if self.name.isupper():
            self.features.append('ALL_CAPS')
        if self.name[0].isupper():
            self.features.append('first_Cap')


## Sentence is Sentence()
## interp. a sentence from a text. 'words' field is a ListOfWord, initially empty.
class Sentence:

    def __init__(self):
        word1 = Word('<start>', '|O', '|O')
        self.words = [word1]

    ## Word -> Void
    ## append the word to the 'words' field
    def add_word(self, word):
        self.words.append(word)

    ## Void -> Void
    ## extract features of words in the sentence and append them to their 'features' field
    def extract_features(self):
        for i in range(len(self.words)):
            self.words[i].extract_word_features()
            if i == 0:
                self.words[i].features.extend(['pre_word_2=<s>', 'pre_word_1=<s>'])
            elif i == 1:
                self.words[i].features.extend(['pre_word_2=<s>', 'pre_word_1=' + self.words[0].name.lower()])
            else:
                self.words[i].features.extend(['pre_word_2=' + self.words[i-2].name.lower(), 'pre_word_1=' + self.words[i-1].name.lower()])
                self.words[i].features.extend(['pre_bigram=' + (self.words[i-1].name + self.words[i].name).lower()])
            try:
                self.words[i].features.append('post_word=' + self.words[i+1].name.lower())
                self.words[i].features.extend(['post_bigram=' + (self.words[i].name + self.words[i+1].name).lower()])
            except IndexError:
                self.words[i].features.append('post_word=</s>')


    ## String -> Int Int Int Int
    ## given a POS tag, return a tuple with true positives, true negatives,
    ## false positives, false negatives for that POS
    def generate_confusion_matrix(self, pos):
        tp, tn, fp, fn = 0.0, 0.0, 0.0, 0.0
        for word in self.words:
            if word.gold_tag == pos:
                if word.classified_correctly():
                    tp += 1
                else:
                    fn += 1
            elif word.pred_tag == pos:
                if not word.classified_correctly():
                    fp += 1
            else:
                tn += 1
        return tp, tn, fp, fn


## Collection is Collection(List of Sentences, List of Strings)
## interp. a list of sentences (a text) together with a list of all
## part-of-speech tags (both gold and predicted) observed in sentences
class Collection:

    ## Void -> Void
    def __init__(self, sentences, parts_of_speech):

        self.sentences = sentences
        self.parts_of_speech = parts_of_speech

    ## Void -> (generator Word)
    def __iter__(self):
        """Yield all words contained in the collection."""
        for sentence in self.sentences:
            for word in sentence.words:
                yield word

    ## String -> Int Int Int Int
    ## given a POS tag, return a list with true positives, true negatives,
    ## false positives, false negatives for that POS
    def generate_confusion_matrix(self, pos):
        tp_tn_fp_fn = numpy.array([0.0, 0.0, 0.0, 0.0])
        for sentence in self.sentences:
            tp_tn_fp_fn += numpy.array(sentence.generate_confusion_matrix(pos))
        return tp_tn_fp_fn.tolist()
            
                    
    def sequential_confusion_matrix(self):
        tp_tn_fp_fn = numpy.array([0.0, 0.0, 0.0, 0.0])
        for sentence in self.sentences:
            matches_gold, matches_pred = [], []
            for word in sentence.words:
                #check gold tags
                if word.gold_tag[1] != 'O':
                    if word.gold_tag[1] == 'B':
                        matches_gold = []
                    if word.pred_tag == word.gold_tag:
                        matches_gold.append(True)
                    else:
                        matches_gold.append(False)
                else:
                    if word.pred_tag == word.gold_tag: #maybe we don't need true negatives  
                        tp_tn_fp_fn[1] += 1   
                    if matches_gold != []:
                        if all(matches_gold):
                            tp_tn_fp_fn[0] += 1
                        else:
                            tp_tn_fp_fn[3] += 1
                        matches_gold = []
                #check pred tags
                if word.pred_tag[1] != 'O':
                    if word.pred_tag[1] == 'B':
                        matches_pred = []
                    if word.pred_tag != word.gold_tag:
                        matches_pred.append(False)
                elif matches_pred != []:
                    tp_tn_fp_fn[2] += 1
                    matches_pred = []

            if matches_gold != []:
                if all(matches_gold):
                    tp_tn_fp_fn[0] += 1
                else:
                    tp_tn_fp_fn[3] += 1
                matches_gold = []
            if matches_pred != []:
                tp_tn_fp_fn[2] += 1
                matches_pred = []            

        return tp_tn_fp_fn.tolist()                        


## =================
## Functions:


## String String -> Collection
## Given the names of the gold standard tokens file and predicted tokens file,
## creates Word class objects and Sentence class objects from the two files (matching in
## length and wordforms). Returns a Collection of all sentences from the files.


def readFiles1(filename):
    print('Reading file from ' + filename)
    listSen = []
    listPOS = []
    filename = open(filename)
    sentence = Sentence()
    i=0
    for line in filename:
        i+=1
        if line != '\n':
            line = line.strip()
            wordform1, tag = line.split('\t')
            word = Word(wordform1, tag, '')
            sentence.add_word(word)
            if tag not in listPOS:
                listPOS.append(tag)
        else:
            listSen.append(sentence)
            sentence.extract_features()
            sentence = Sentence()
        if i%10000 == 0:
            print('Features extracted, i=' + str(i))
    listSen.append(sentence)
    sentence.extract_features()
    filename.close()
    return Collection(listSen, listPOS)

